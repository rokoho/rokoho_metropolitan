
// Template Metropolitan javascript functions

( function($) {
	$(window).resize(function(){

		$('.op_panel') .css({'min-height': (($(window).height()))+'px'});

		$('[data-spy="scroll"]').each(function () { // Refresh Bootstrap Scroll-Spy
			  var $spy = $(this).scrollspy('refresh')
		})	
	});

$(document).ready(function() {

	$('.op_panel') .css({'min-height': (($(window).height()))+'px'});
    // $('#panel-0  .container.item-wrap').flexVerticalCenter();


// waypoints

  $('#panel-2').waypoint(function(direction) {

   if (direction == 'down') {
       $('#albumCarousel div.col-md-4 img').animate({padding:"5px"});
    }
    else {
       $('#albumCarousel div.col-md-4 img').animate({padding:"0px"});
    }
	}, {offset: 200});
	
// Smoothscroll

	$('a.smoothscroll').smoothScroll({
	  
		afterScroll: function() {$(".navbar-collapse").hasClass( "in" ).collapse('hide');},
		easing: 'swing',
		speed: 900,
		offset:10
	});

// Refresh sbootstrap scrollspy

	$('[data-spy="scroll"]').each(function () {

		var $spy = $(this).scrollspy('refresh')

	})

switch_style( style ); return false;

// $('.content').on('click', function(){ $(".navbar-collapse").collapse('hide'); });

});
} ) ( jQuery );