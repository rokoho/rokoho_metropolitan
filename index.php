<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.Metropolitan
 *
 * @copyright   Copyright (C) 2014 Rokoho Webdesign.
 */

defined('_JEXEC') or die;


// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
unset($this->_scripts['/budget/media/system/js/mootools-core.js']); 


// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');

if($this->params->get('alt_sitename') !=''):$sitename = $this->params->get('alt_sitename');else:$sitename = $app->getCfg('sitename');endif;


$doc->addScript('templates/' .$this->template. '/js/bootstrap.min.js');

$doc->addStyleSheet('templates/'.$this->template.'/css/bootstrap-min.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('libraries/font-awesome/css/font-awesome.css');


if($this->params->get('tlogo')):
	$logo = '<img src="' . $this->params->get('tlogo') . '" class="img-circle" alt="' . $sitename . '" />';
endif;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
  <script> var style_cookie_name = '<?php echo $this->template; ?>';</script>
<jdoc:include type="head" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="templates/<?php echo $this->template ?>/ico/favicon.png" />
<!--[if IE]>
  <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet" type="text/css">
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript" src="templates/<?php echo $this->template ?>/js/jquery.smooth-scroll.min.js"></script>
  <script type="text/javascript" src="templates/<?php echo $this->template ?>/js/waypoints.min.js"></script>
  <script type="text/javascript" src="templates/<?php echo $this->template ?>/js/jquery.flexverticalcenter.js"></script>
  <script type="text/javascript" src="templates/<?php echo $this->template ?>/js/template.js"></script>
</head>
<body data-spy="scroll" data-target=".navbar"  >
<i id="ltop"></i>
<div class="navbar-wrapper">
  <div class="">
    <div class="navbar navbar-inverse navbar-fixed-top navbar-right" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand smoothscroll" href="#ltop">
            <span class="logo"></span><?php // echo $logo; ?>&nbsp;<?php // echo $sitename; ?>

          </a>
        </div>
        <div class="navbar-collapse collapse">
          <jdoc:include type="modules" name="collapse" />
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div id="headerCarousel" class="carousel slide" data-ride="carousel"> 
   Indicators 
  <ol class="carousel-indicators">
    <li data-target="#headerCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#headerCarousel" data-slide-to="1"></li>
    <li data-target="#headerCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo $this->params->get('slider_one'); ?>" alt="<?php echo $sitename; ?>" />
      <div class="container">
        <div class="carousel-caption">
          <h1><?php echo $this->params->get('slider_one_title'); ?></h1>
          <?php echo $this->params->get('slider_one_text'); ?>
            <p>
            <a class="btn btn-lg btn-danger smoothscroll" href="#contact" role="button">Contact us today!</a>
          </p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="<?php echo $this->params->get('slider_two'); ?>" alt="<?php echo $sitename; ?>" />
      <div class="container">
        <div class="carousel-caption">
          <h1><?php echo $this->params->get('slider_two_title'); ?></h1>
          <?php echo $this->params->get('slider_two_text'); ?>
          <p>
            <a class="btn btn-lg btn-danger smoothscroll" href="#portfolio" role="button">Our portfolio</a>
          </p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="<?php echo $this->params->get('slider_three'); ?>" alt="<?php echo $sitename; ?>" />
      <div class="container">
        <div class="carousel-caption">
          <h1><?php echo $this->params->get('slider_three_title'); ?></h1>
          <?php echo $this->params->get('slider_three_text'); ?>
          <p>
            <a class="btn btn-lg btn-danger smoothscroll" href="#features" role="button">Get started</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#headerCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" href="#headerCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>-->
<?php if (count(JFactory::getApplication()->getMessageQueue())) : ?>
<div class="container message">
  <jdoc:include type="message" />
</div>
<?php endif; ?>
<div class="content <?php if($params->get('bscontainer') 
|| $_SERVER['REQUEST_URI'] == '/budget/ifixit'
|| $_SERVER['REQUEST_URI'] == '/budget/ict-nieuws.html'

){echo 'container';} ?>">
  <div class="item-wrapper">
    <jdoc:include type="component" />
  </div>
  <hr class="featurette-divider">
  <jdoc:include type="modules" name="postcontent" style="html5" />
</div>
  <footer>
    <div class="container">
      <p class="text-center"><a href="#ltop" class="smoothscroll"><i class="fa fa-chevron-circle-up fa-3x"></i></a></p>
      <jdoc:include type="modules" name="footer" style="html5" />
    </div>
  </footer>
<script>
 // Mootools-more - Bootstrap carousel fix
 
 if (typeof jQuery != 'undefined' && typeof MooTools != 'undefined' ) {
    Element.implement({
        slide: function(how, mode){
            return this;
        }
    });
}
</script><?php // var_dump($_SERVER); ?>
</body>
</html>